import { Component } from '@angular/core';
import { Platform, IonicPage, NavController, NavParams, ToastController, ModalController } from 'ionic-angular';
import { ApiServiceProvider } from '../../../../providers/api-service/api-service';
import { NotifModalPage } from './notif-modal';

@IonicPage()
@Component({
  selector: 'page-notif-setting',
  templateUrl: 'notif-setting.html',
})
export class NotifSettingPage {
  islogin: any;
  dataOriginal: any;
  fData: any = {};
  notifType: any;
  isAddEmail: boolean = false;
  emailList: any;
  isAddPhone: boolean = false;
  phonelist: any;
  shownotifdiv: boolean = true;
  newArray: any = [];
  lt: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apiCall: ApiServiceProvider,
    private toastCtrl: ToastController,
    private modalCtrl: ModalController,
    public platform: Platform,
    // private nativeAudio: NativeAudio
  ) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("user details=> ", JSON.stringify(this.islogin));
    this.getCustDetails();

  }

  getCustDetails() {
    let _bUrl = this.apiCall.mainUrl + 'users/getCustumerDetail?uid=' + this.islogin._id;
    this.apiCall.startLoading().present();
    this.apiCall.getSOSReportAPI(_bUrl)
      .subscribe(respData => {
        this.apiCall.stopLoading();
        this.lt = respData.cust.pass;
        localStorage.setItem('pwd2', this.lt);
        if (respData.length === 0) {
          return;
        } else {
          if (respData.cust.alert) {
            var result = Object.keys(respData.cust.alert).map(function (key) {
              return [key, respData.cust.alert[key]];
            });
            this.newArray = result.map(function (r) {
              return {
                key: r[0],
                imgUrl: 'assets/notificationIcon/' + r[0] + '.png',
                keys: r[1]
              }
            });

            console.log("someArr: " + this.newArray)
          }
        }
      },
        err => {
          this.apiCall.stopLoading();
        });
  }

  ionViewDidEnter() {
    console.log('ionViewDidEnter NotifSettingPage');
  }

  onChange(ev) {
    console.log("event: ", ev)
  }

  tab(key1, key2) {
    if (key1 && key2) {
      if (key1.keys[key2] === false) {
        key1.keys[key2] = false;
      } else {
        key1.keys[key2] = true;
      }
    }
    for (var t = 0; t < this.newArray.length; t++) {
      if (this.newArray[t].key === key1.key) {
        this.newArray[t].keys = key1.keys;
      }
    }
    var temp = [];
    for (let e = 0; e < this.newArray.length; e++) {
      temp.push({
        [this.newArray[e].key]: this.newArray[e].keys,
        key: this.newArray[e].key
      });
    }
    /* covert an array into object */
    var res = {};
    temp.forEach(function (a) { res[a.key] = a[a.key] })

    this.fData.contactid = this.islogin._id;
    this.fData.alert = res;
    let url = this.apiCall.mainUrl + 'users/editUserDetails';
    this.apiCall.startLoading().present();
    this.apiCall.urlpasseswithdata(url, this.fData)
      .subscribe(respData => {
        this.apiCall.stopLoading();
        console.log("check stat: ", respData);
        if (respData) {
          this.toastCtrl.create({
            message: 'Setting Updated',
            duration: 1500,
            position: 'bottom'
          }).present();
        }
      },
        err => {
          this.apiCall.stopLoading();
        });
  }

  addEmail(noti) {
    // use AudioProvider to control selected track

    this.notifType = noti.key;
    this.isAddEmail = true;
    var data = {
      "buttonClick": 'email',
      "notifType": this.notifType,
      "compData": {
        [this.notifType]: noti.keys
      },
      "newArray":this.newArray
    }

    const modal = this.modalCtrl.create(NotifModalPage, {
      "notifData": data
    });
    modal.present();
  }

  addPhone(noti) {
    this.notifType = noti.key;
    this.isAddPhone = true;

    var data = {
      "buttonClick": 'phone',
      "notifType": this.notifType,
      "compData": {
        [this.notifType]: noti.keys
      },
      "newArray":this.newArray
    }

    const modal = this.modalCtrl.create(NotifModalPage, {
      "notifData": data
    });
    modal.present();
  }
}
