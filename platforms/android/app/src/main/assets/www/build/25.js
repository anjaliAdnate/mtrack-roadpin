webpackJsonp([25],{

/***/ 662:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TtPageModule", function() { return TtPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__tt__ = __webpack_require__(780);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var TtPageModule = /** @class */ (function () {
    function TtPageModule() {
    }
    TtPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__tt__["a" /* TtPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__tt__["a" /* TtPage */]),
            ],
        })
    ], TtPageModule);
    return TtPageModule;
}());

//# sourceMappingURL=tt.module.js.map

/***/ }),

/***/ 780:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TtPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_themeable_browser__ = __webpack_require__(441);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var iabRef = null;
var TtPage = /** @class */ (function () {
    function TtPage(navCtrl, navParams, apiservice, alerCtrl, themeableBrowser) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiservice = apiservice;
        this.alerCtrl = alerCtrl;
        this.themeableBrowser = themeableBrowser;
        this.iabRef = null;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("islogin devices => " + JSON.stringify(this.islogin));
        this.payment();
    }
    TtPage.prototype.insertMyHeader = function () {
        iabRef.executeScript({
            code: "var b=document.querySelector('body'); var a=document.createElement('div');document.createTextNode('my own header!'); a.appendChild(newContent);b.parentNode.insertBefore(a,b);"
        }, function () {
            alert("header successfully added");
        });
    };
    TtPage.prototype.iabClose = function () {
        iabRef.removeEventListener('exit', this.iabClose);
    };
    TtPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TtPage');
    };
    TtPage.prototype.call = function () {
        if (this.resp == undefined) {
            var alerttemp = this.alerCtrl.create({
                message: "invalid-args",
                buttons: [{
                        text: 'Okay'
                    }]
            });
            alerttemp.present();
        }
        else {
            var options = {
                statusbar: {
                    color: '#f54278'
                },
                toolbar: {
                    height: 50,
                    color: '#d80622'
                },
                title: {
                    color: '#ffffff',
                    showPageTitle: true
                },
                backButton: {
                    wwwImage: '/assets/icon/arrow.png',
                    imagePressed: 'back_pressed',
                    align: 'left',
                    // event: 'backPressed'
                    event: 'closePressed'
                },
                backButtonCanClose: true
            };
            // const browser: ThemeableBrowserObject = this.themeableBrowser.create(this.resp, '_self', options);
            this.themeableBrowser.create(this.resp, '_self', options);
        }
    };
    TtPage.prototype.payment = function () {
        var _this = this;
        console.log("inside the payment");
        var url = "https://www.oneqlik.in/pullData/getSetuLink?mobileNumber=" + this.islogin.phn;
        this.apiservice.startLoading().present();
        this.apiservice.getdevicesForAllVehiclesApi(url)
            .subscribe(function (resp) {
            _this.apiservice.stopLoading();
            console.log("server image url=> ", resp);
            console.log("chk response", resp);
            _this.resp = resp.link;
            console.log("final link", _this.resp);
            if (_this.resp == undefined) {
                var alerttemp = _this.alerCtrl.create({
                    message: "invalid-args",
                    buttons: [{
                            text: 'Okay'
                        }]
                });
                alerttemp.present();
            }
        }, function () {
            _this.apiservice.stopLoading();
        });
        this.apiservice.payment(this.data);
    };
    TtPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-tt',template:/*ion-inline-start:"/Users/apple/Desktop/oneqlik-projects/oneqlik/src/pages/tt/tt.html"*/'\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>Payment</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n  <ion-card>\n\n    <ion-card-header>\n\n      <img src="assets/imgs/download.png">\n\n    </ion-card-header>\n\n    <ion-card-content>\n\n      <button ion-button full color="gpsc" (click)="call()">Pay</button>\n\n    </ion-card-content>\n\n\n\n  </ion-card>\n\n<!-- <p (click)="call()">Test</p>\n\n<a href="resp">{{ resp }}</a> -->\n\n</ion-content>\n\n'/*ion-inline-end:"/Users/apple/Desktop/oneqlik-projects/oneqlik/src/pages/tt/tt.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_themeable_browser__["a" /* ThemeableBrowser */]])
    ], TtPage);
    return TtPage;
}());

//# sourceMappingURL=tt.js.map

/***/ })

});
//# sourceMappingURL=25.js.map